<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class SuperAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $admin = User::create([
            'name'=>'barry',
            'firstname'=>'thierno',
            'username'=>'admin',
            'password'=>bcrypt('administrateur'),
            'email'=>'example@gmail.com',
            'telephone'=>"90-30-13-99",
            'type'=>"Admin"

        ]);
        $role1 = Role::find(1);
        $admin->assignRole($role1);

    }
}
