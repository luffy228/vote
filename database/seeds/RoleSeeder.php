<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('roles')->insertOrIgnore([
            ['name' => 'SuperAdministrateur','guard_name'=>'web'],
            ['name' => 'Deputer','guard_name'=>'web'],
        ]);

    }
}
