<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('votes', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
        });

        if (Schema::hasTable('votes')) {
            Schema::table('votes',function(Blueprint $table){
                 if (!Schema::hasColumn('votes', 'sujet_id')) {
                    $table->unsignedBigInteger('sujet_id')->nullable();
                    $table->foreign('sujet_id')->references('id')->on('sujets');
                  }
                  if (!Schema::hasColumn('votes', 'choix')) {
                    $table->string('choix',150);
                 }
                 if (!Schema::hasColumn('votes', 'votant_id')) {
                    $table->unsignedBigInteger('votant_id')->nullable();
                    $table->foreign('votant_id')->references('id')->on('deputers');
                 }
            });
        }

        if (!Schema::hasTable('votes')) {
            Schema::create('votes', function (Blueprint $table) {
                $table->id();
                $table->unsignedBigInteger('sujet_id')->nullable();
                $table->foreign('sujet_id')->references('id')->on('sujets');
                $table->string('choix',150);
                $table->unsignedBigInteger('votant_id')->nullable();
                $table->foreign('votant_id')->references('id')->on('deputers');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('votes');
    }
}
