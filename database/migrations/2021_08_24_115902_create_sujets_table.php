<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSujetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (Schema::hasTable('sujets')) {
            Schema::table('sujets',function(Blueprint $table){
                 if (!Schema::hasColumn('sujets', 'objet')) {
                    $table->string('objet',150);

                  }

                 if (!Schema::hasColumn('sujets', 'description')) {
                    $table->string('description',150);
                 }

                 if (!Schema::hasColumn('sujets', 'vote')) {
                    $table->string('vote',150);
                 }
            });
        }

        if (!Schema::hasTable('sujets')) {
            Schema::create('sujets', function (Blueprint $table) {
                $table->id();
                $table->string('objet',150);
                $table->string('description',150);
                $table->string('vote',150);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sujets');
    }
}
