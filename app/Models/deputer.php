<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class deputer extends Model
{
    //
    public function userMother()
    {
        return $this->morphOne('App\Models\User','userType');
    }

    public function nameModel()
    {
        return 'deputer';
    }

}
