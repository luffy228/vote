<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class sujet extends Model
{
    //
    protected $fillable = ['objet','description','vote'];
    public function nameModel()
    {
        return 'sujet';
    }
}
