<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class vote extends Model
{
    //
    protected $fillable = ['sujet_id','choix','votant_id'];
    public function nameModel()
    {
        return 'vote';
    }
}
