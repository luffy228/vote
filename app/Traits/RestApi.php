<?php

namespace App\Traits;

use GuzzleHttp\Client;

trait RestApi
{

    protected function url()
    {
        //$url = "http://voteapi.xaed4360.odns.fr/api/v1/";
        $url = "http://192.168.1.101:8000/api/v1/";
        return $url;
    }

    protected function loginurl()
    {
        $lien = 'login';
        $url = $this->url().$lien;
        return $url;
    }

    protected function listAdministrateur()
    {
        $lien = 'administrateur/all';
        $url = $this->url().$lien;
        return $url;
    }

    protected function addAdministrateur()
    {
        $lien = 'administrateur/add';
        $url = $this->url().$lien;
        return $url;
    }

    protected function listeVote()
    {
        $lien = 'vote/listByAdministrateur';
        $url = $this->url().$lien;
        return $url;
    }

    protected function listeTypeVote()
    {
        $lien = 'typeVote/all';
        $url = $this->url().$lien;
        return $url;
    }

    protected function listeTypeVotant()
    {
        $lien = 'typeVotant/all';
        $url = $this->url().$lien;
        return $url;
    }

    protected function addTypeVotant()
    {
        $lien = 'typeVotant/add';
        $url = $this->url().$lien;
        return $url;
    }

    protected function addVoteAdmin()
    {
        $lien = 'vote/add';
        $url = $this->url().$lien;
        return $url;
    }

    protected function informationvote()
    {
        $lien = 'vote/information';
        $url = $this->url().$lien;
        return $url;
    }

    protected function addingCandidatVote()
    {
        $lien = 'candidat/add';
        $url = $this->url().$lien;
        return $url;
    }








}
