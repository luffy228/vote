<?php

namespace App\Http\Controllers;

use App\Repositories\Implementation\AgentRepository;
use App\Repositories\Implementation\UserRepository;
use App\Traits\RestApi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session as FacadesSession;


class HomeController extends Controller
{
    use RestApi;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function index()
    {
        return view('welcome');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function adminPage(Request $request)
    {


        $url = $this->loginurl();

            $response = Http::acceptJson()->post($url,[
                'phone'=> $request["phone"],
                'password'=>$request["password"]
            ]);
            $res = json_decode($response->body()) ;

            if ($res->status == "success") {
                FacadesSession::put('token', $res->data->token);
                FacadesSession::put('infoUser', $res->data->user);
                FacadesSession::put('role', $res->data->role[0]);

                return redirect()->to('vote/home');
            } else {
                return redirect()->back();
            }

    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }
}
