<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Repositories\Implementation\SujetRepository;
use App\Traits\RestApi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;

class VoteController extends Controller
{
    use RestApi;


    public function index(Request $request)
    {
        $infoUser =Session::get('infoUser');
        $infoRole = Session::get('role');
        $url = $this->listeVote();
        $token = Session::get('token');
        $response = Http::withToken($token)->acceptJson()->get($url);
        $res = json_decode($response->body()) ;
        if ($res->status == "success") {
            $listvote = $res->data;
            return view('pages.ListVote', compact('infoUser','infoRole','listvote'));
        }


    }

    public function addingCandidat($vote , $categorie)
    {
        $infoUser =Session::get('infoUser');
        $infoRole = Session::get('role');
        return view('pages.AjouterCandidat', compact('infoUser','infoRole','vote','categorie'));


    }

    public function addCandidat(Request $request)
    {
        $url = $this->addingCandidatVote();
        $token = Session::get('token');
        $response = Http::withToken($token)->acceptJson()->post($url,
        [
            "countCandidat"=>$request["countCandidat"],
            "vote"=>$request["vote"],
            "nom"=>$request["nom"],
            "age"=>$request["age"],
            "image"=>$request["image"],
            "categorie"=>$request["categorie"],
        ]);
        $res = json_decode($response->body()) ;
        if ($res->status == "success") {
            return redirect()->to('superadmin/listVote');
        }


    }



    public function information(Request $request)
    {
        $infoUser =Session::get('infoUser');
        $infoRole = Session::get('role');
        $url = $this->informationvote();

        $response = Http::acceptJson()->post($url,[
            "vote"=>$request["vote"],
        ]);
        $res = json_decode($response->body()) ;

        if ($res->status == "success") {
            $information = $res->data;
            //dd($information);
            return view('informationvote', compact('infoUser','infoRole','information'));
        }


    }

    public function store(Request $request)
    {
        $url = $this->addVoteAdmin();
        $token = Session::get('token');
        if ($request["typeVote"] == 1) {
            $form_request = [
                "typeVotant"=>$request["typeVotant"],
                "typeVote"=>$request["typeVote"],
                "nomSujet"=>$request["nomSujet"],
                "intitule"=>$request["intitule"],
            ];
        }
        if ($request["countCategorie"] == 0  &&  $request["typeVote"] == 2) {
            $form_request = [
                "typeVotant"=>$request["typeVotant"],
                "typeVote"=>$request["typeVote"],
                "countCategorie"=>$request["countCategorie"],
                "intitule"=>$request["intitule"],
                "nomCategorie"=>$request["intitule"],
            ];
        }

        if ($request["countCategorie"] > 0  &&  $request["typeVote"] == 2) {
            $form_request = [
                "typeVotant"=>$request["typeVotant"],
                "typeVote"=>$request["typeVote"],
                "countCategorie"=>$request["countCategorie"],
                "intitule"=>$request["intitule"],
                "nomCategorie"=>$request["nomCategorie"],
            ];
        }
        $response = Http::withToken($token)->acceptJson()->post($url,$form_request);
        $res = json_decode($response->body()) ;

        if ($res->status == "success") {
            return redirect()->to('superadmin/listVote');
        } else {
            return redirect()->back();
        }

    }

    public function ajoutVote()
    {
        $infoUser =Session::get('infoUser');
        $infoRole = Session::get('role');
        $url = $this->listeTypeVotant();
        $response = Http::acceptJson()->get($url);
        $res = json_decode($response->body()) ;

        $url2 = $this->listeTypeVote();
        $response2 = Http::acceptJson()->get($url2);
        $res2 = json_decode($response2->body()) ;

        if ($res->status == "success" && $res2->status == "success") {
            $listtypevotant = $res->data;
            $listtypevote = $res2->data;
            return view('pages.AjouterVote', compact('infoUser','infoRole','listtypevote', 'listtypevotant'));

        }

    }

}
