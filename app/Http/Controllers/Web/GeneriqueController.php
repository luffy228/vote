<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Repositories\Implementation\UserRepository;
use App\Traits\RestApi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;

class GeneriqueController extends Controller
{
    use RestApi;
    //


    public function index()
    {
        // definir le role de l'utilisateur
        $infoUser =Session::get('infoUser');
    $infoRole = Session::get('role');

    if ($infoRole == "SuperAdministrateur") {
        $url = $this->listAdministrateur();
        $response = Http::acceptJson()->get($url);
        $res = json_decode($response->body()) ;
        if ($res->status == "success") {
            $listadmin = $res->data;
            return view('home', compact('infoUser','infoRole','listadmin'));


        }
    }

    if ($infoRole == "Administrateur") {
        $url = $this->listAdministrateur();
        $response = Http::acceptJson()->get($url);
        $res = json_decode($response->body()) ;
        if ($res->status == "success") {
            $listadmin = $res->data;
            return view('home', compact('infoUser','infoRole','listadmin'));


        }
    }




    }

    public function profile()
    {
        return view('profile');
    }

    public function updateCompte(Request $request)
    {
        $data = [];
        if ($request["name"] != null) {
            $data["name"] = $request["name"];
        }
        if ($request["firstname"] != null) {
            $data["firstname"] = $request["firstname"];
        }
        if ($request["phone"] != null) {
            $data["telephone"] = $request["phone"];
        }
        if ($request["username"] != null) {
            $data["username"] = $request["username"];
        }
        if ($request["email"] != null) {
            $data["email"] = $request["email"];
        }

        //$this->userRepo->update($data,Auth::user()->id);
        return redirect()->back();
    }

    public function updatePassword(Request $request)
    {

        //$data = [];
        if ($request["lastpassword"] == null) {
         //   return redirect()->back();
        }


        if (Hash::check($request["lastpassword"], Auth::user()->password)) {
           // $data["password"] = bcrypt($request["newpassword"]);
            //$this->userRepo->update($data,Auth::user()->id);
            //return redirect()->back();
        }
        return redirect()->back();


    }


}
