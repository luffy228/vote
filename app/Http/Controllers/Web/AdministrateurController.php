<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Traits\RestApi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;

class AdministrateurController extends Controller
{
    use RestApi;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $infoUser =Session::get('infoUser');
        $infoRole = Session::get('role');

        $url = $this->listAdministrateur();
        $response = Http::acceptJson()->get($url);
        $res = json_decode($response->body()) ;
        if ($res->status == "success") {
            $listadmin = $res->data;
            return view('pages.ListAdministrateur', compact('infoUser','infoRole', 'listadmin'));


        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $url = $this->addAdministrateur();

        $response = Http::acceptJson()->post($url,[
            'phone'=> $request["phone"],
            'password'=>$request["password"]
        ]);
        $res = json_decode($response->body()) ;

        if ($res->status == "success") {
            return redirect()->to('superadmin/listAdministrateur');
        } else {
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
