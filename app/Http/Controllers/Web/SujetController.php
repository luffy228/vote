<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Repositories\Implementation\SujetRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Laravel\Ui\Presets\React;

class SujetController extends Controller
{



    protected $sujetRepo;
    public function __construct(App $app)
    {
        $this->middleware('auth');
        $this->sujetRepo = new SujetRepository($app);
    }

    public function listSujet()
    {
        $sujet = $this->sujetRepo->getall();
        return view('pages.ListSujet',compact('sujet'));

    }

    public function sujetVote()
    {
        // choisir les sujets non voter
        $sujet = $this->sujetRepo->SujetNonVoter();
        return view('pages.SujetVote',compact('sujet'));

    }

    public function addSujet(Request $request)
    {
        $form_request = [
            'objet'=>$request["objet"],
            'description'=>$request["description"],
            'vote'=>"Non",
        ];
        $this->sujetRepo->create($form_request);
        return redirect()->route('listesujet');
    }

    public function ChooseSujet(Request $request)
    {
        $form_request = [
            'vote'=>"Oui",
        ];
        $this->sujetRepo->update($form_request,$request["sujet"]);
        // return vers la page de vote
        return redirect()->route('resultsujet',["sujet"=>$request["sujet"]]);

    }
}
