<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AjaxController extends Controller
{


    public function ecran(Request $request)
    {

        $fichier = $request['fichier'];
        $val = $request['val'];
        $param = $request['param'];

        return view($fichier,array('val'=>$val,'param' =>$param));
    }
}
