<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Repositories\Implementation\AgentRepository;
use App\Repositories\Implementation\ArrondissementRepository;
use App\Repositories\Implementation\UserRepository;
use App\Repositories\Implementation\VotantRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    //
    protected $userRepo;
    protected $deputerRepo;
    protected $agentRepo;
    protected $arrondissementRepo;
    public function __construct(App $app)
    {
        $this->middleware('auth');
        $this->userRepo = new UserRepository($app);
        $this->deputerRepo = new VotantRepository($app);
    }


    public function listDeputer()
    {
        $deputer = $this->deputerRepo->information();
        return view('pages.ListDeputer',compact('deputer'));
    }

    public function addDeputer(Request $request)
    {
                $deputer_request=[];
                $debuter = $this->deputerRepo->create($deputer_request);
                $form_request = [
                    'name'=>$request["name"],
                    'firstname'=>$request["firstname"],
                    'username'=>$request["telephone"],
                    'password'=>bcrypt($request["password"]),
                    'email'=>$request["email"],
                    'telephone'=>$request["telephone"],
                    'type'=>"Deputer",
                    'user_type'=>$this->deputerRepo->model(),
                    'user_id'=>$debuter['id']
                ];
                $user = $this->userRepo->create($form_request);

                $role1 = Role::find(2);
                $user->assignRole($role1);
                return redirect()->route('listedeputer');

    }






}
