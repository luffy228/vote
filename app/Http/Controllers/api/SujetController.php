<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Repositories\Implementation\SujetRepository;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class SujetController extends Controller
{
    //
    use ApiResponser;
    protected $sujetRepo;
    function __construct(App $app)
    {
        $this->sujetRepo = new SujetRepository($app);
    }

    public function listSujet()
    {
        $sujet = $this->sujetRepo->getall();
        return $this->successResponse($sujet);
    }

    public function canVote(Request $request)
    {
        $sujet = $this->sujetRepo->show($request["id"]);

        if ($sujet["vote"] == "Non" || $sujet["vote"] == "Voter") {
            $data = [
                'CanVote'=>"Non"
            ];
        }
        if ($sujet["vote"] == "Oui") {
            $data = [
                'CanVote'=>"Oui"
            ];
        }
        return $this->successResponse($data);
    }
}
