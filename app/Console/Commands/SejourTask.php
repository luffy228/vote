<?php

namespace App\Console\Commands;

use App\Models\Alerte;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SejourTask extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sejour:task';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verify if the sejour is expired or not and create and alerte for expired sejour';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $today = date('Y-m-d',strtotime('now'));

        $sejourAlerte = DB::table('alertes')->join('observations','alertes.observations_id','=','observations.id')
                                ->where('observations.motifs_id',3)
                                ->select('observations.id')
                                ->get();
        $listeAlerte = [];
        foreach ($sejourAlerte as $alertes) {
            array_push($listeAlerte,$alertes->id);
        }
        $record = DB::table('observations')
                        ->whereIn('motifs_id',['3'])
                        ->whereNotIn('observations.id',$listeAlerte)
                        ->where('observations.finsejour','<',$today)
                        ->get();
        foreach ($record as $observation) {
            $agents = $observation->agents_id;
            $observation_id = $observation->id;
            $sejour = new Alerte();
            $sejour->statut = 0;
            $sejour->agents_id = $agents;
            $sejour->observations_id = $observation_id;
            $sejour->save();
        }

        $this->info('Successfully');
    }
}
