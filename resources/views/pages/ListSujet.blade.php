@extends('layouts.app')

@section('contenu')
    <div class="main-panel">
        <div class="content">
            <div class="page-inner">
                <div class="page-header">
                    <h4 class="page-title">Liste des sujets</h4>
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="{{ route('home') }}">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('listesujet') }}">Liste des Sujets</a>
                        </li>

                    </ul>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="d-flex align-items-center">
                                    <h4 class="card-title">Ajouter un sujet</h4>
                                    <button class="btn btn-primary btn-round ml-auto" data-toggle="modal" data-target="#exampleModal">
                                        <i class="fa fa-plus"></i>
                                        Ajouter
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <!-- Modal -->
                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Ajouter un administrateur</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            </div>
                                            <form  action="{{route('addsujet')}}" method="post">
                                                @csrf
                                            <div class="modal-body">



                                                    <div class="form-group">
                                                        <label for="name" class="col-form-label">Objet:</label>
                                                        <input type="text" class="form-control" name="objet" required>

                                                    </div>

                                                    <div class="form-group">
                                                        <label for="firstname" class="col-form-label">Description:</label>
                                                        <textarea class="form-control" id="comment" name="description" rows="5">
                                                        </textarea>
                                                    </div>



                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn  btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn  btn-primary">Ajouter</button>
                                            </div>
                                        </form>
                                        </div>
                                    </div>
                                </div>


                                <div class="table-responsive">
                                    <table id="add-row" class="display table table-striped table-hover" >
                                        <thead>
                                            <tr>
                                                <th>Objet</th>
                                                <th>Description</th>
                                                <th>Statut</th>
                                                <th>Resultat</th>
                                                <th style="width: 10%">Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Objet</th>
                                                <th>Description</th>
                                                <th>Statut</th>
                                                <th>Resultat</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            @foreach ($sujet as $sujet)
                                            <tr>
                                                <td>{{$sujet->objet}}</td>
                                                <td>{{$sujet->description}}</td>
                                                <td>{{$sujet->vote}}</td>
                                                <td>{{$sujet->objet}}</td>
                                                <td>
                                                    <div class="form-button-action">
                                                        <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Supprimer">
                                                            <i class="fa fa-times"></i>
                                                        </button>
                                                    </div>
                                                </td>

                                            </tr>

                                        @endforeach


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection




@section('js_special')

<!-- Page level custom scripts -->

<script>

</script>
<script >
    $(document).ready(function() {

        // Add Row
        $('#add-row').DataTable({
            "pageLength": 5,
        });

        var action = '<td> <div class="form-button-action"> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"> <i class="fa fa-edit"></i> </button> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove"> <i class="fa fa-times"></i> </button> </div> </td>';

        $('#addRowButton').click(function() {
            $('#add-row').dataTable().fnAddData([
                $("#addName").val(),
                $("#addPosition").val(),
                $("#addOffice").val(),
                action
                ]);
            $('#addRowModal').modal('hide');

        });
    });
</script>


@endsection









