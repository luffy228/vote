@extends('layouts.app')

@section('contenu')
    <div class="main-panel">
        <div class="content">
            <div class="page-inner">
                <div class="page-header">
                    <h4 class="page-title">Vote</h4>
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="{{ route('home') }}">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('listevote')}}">Ajouter un vote</a>
                        </li>

                    </ul>
                </div>

                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Ajouter un type de votant</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <form  action="{{route('addtypevotant')}}" method="post">
                                @csrf
                            <div class="modal-body">

                                    <div class="form-group">
                                        <label for="message-text" class="col-form-label">Type de votant:</label>
                                        <input type="text" class="form-control" name="type" required>
                                    </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn  btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn  btn-primary">Ajouter</button>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12"> <button class="btn btn-primary btn-round ml-auto" data-toggle="modal" data-target="#exampleModal">
                        <i class="fa fa-plus"></i>
                        Ajouter Type de votant
                    </button>
                        <form  action="{{route('addingvote')}}" method="post">
                            @csrf
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">Information sur le vote   </div>
                            </div>

                            <div class="card-body">
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Type de votant </label>

                                     <!-- Modal -->

                                    <select class="form-control" id="exampleFormControlSelect1" name="typeVotant">
                                        @foreach ($listtypevotant as $listtypevotant)
                                            <option value="{{$listtypevotant->id}}">{{$listtypevotant->type}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Type de vote  </label>
                                    <select onchange="ecran($(this).val(),'information','Typevoteajax')" class="form-control" id="exampleFormControlSelect1" name="typeVote">
                                        <option  selected disabled>Selectionner un type de vote</option>
                                        @foreach ($listtypevote as $listtypevote)
                                            <option value="{{$listtypevote->id}}">{{$listtypevote->type}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group" >
                                    <label for="email2">Nom du vote</label>
                                    <input type="type" name="intitule" class="form-control" id="email2">
                                </div>

                            </div>
                        </div>

                        <div class="card"  id="information">

                        </div>

                        <div class="card-action">
                            <button class="btn btn-success">Submit</button>
                            <button class="btn btn-danger">Cancel</button>
                        </div>
                    </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection




@section('js_special')

<!-- Page level custom scripts -->

<script>

</script>
<script >
    $(document).ready(function() {

        // Add Row
        $('#add-row').DataTable({
            "pageLength": 5,
        });

        var action = '<td> <div class="form-button-action"> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"> <i class="fa fa-edit"></i> </button> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove"> <i class="fa fa-times"></i> </button> </div> </td>';

        $('#addRowButton').click(function() {
            $('#add-row').dataTable().fnAddData([
                $("#addName").val(),
                $("#addPosition").val(),
                $("#addOffice").val(),
                action
                ]);
            $('#addRowModal').modal('hide');

        });
    });
</script>


@endsection









