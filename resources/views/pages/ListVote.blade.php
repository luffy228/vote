@extends('layouts.app')

@section('contenu')
    <div class="main-panel">
        <div class="content">
            <div class="page-inner">
                <div class="page-header">
                    <h4 class="page-title">Vote</h4>
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="{{ route('home') }}">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('listevote')}}">Liste des votes creer</a>
                        </li>

                    </ul>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="d-flex align-items-center">
                                    <h4 class="card-title">Ajouter un vote</h4>
                                    <a href="{{route('addVote')}}">
                                    <button class="btn btn-primary btn-round ml-auto">
                                        <i class="fa fa-plus"></i>
                                        Ajouter
                                    </button>
                                    </a>
                                </div>
                            </div>
                            <div class="card-body">
                                <!-- Modal -->



                                <div class="table-responsive">
                                    <table id="add-row" class="display table table-striped table-hover" >
                                        <thead>
                                            <tr>
                                                <th>Intitule</th>
                                                <th>Votant</th>
                                                <th>Type de vote</th>
                                                <th>statut</th>
                                                <th>details du vote</th>
                                                <th style="width: 10%">Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Intitule</th>
                                                <th>Votant</th>
                                                <th>Type de vote</th>
                                                <th>statut</th>
                                                <th>details du vote</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            @foreach ($listvote as $vote)
                                                <tr>
                                                    <td>{{$vote->intitule}}</td>
                                                    <td>{{$vote->typeVotant}}</td>
                                                    <td>{{$vote->typeVote}}</td>
                                                    <td>{{$vote->statut}} {{$vote->id}}
                                                    <td>
                                                        <!--
                                                        <a href="{{route('informationvote',["id"=>$vote->id])}}">
                                                            <button class="btn btn-success">information</button></td>
                                                        </a>-->
                                                        <form action="{{route('informationvote')}}" method="POST">
                                                            @csrf
                                                            <input type="hidden" name="vote" value={{$vote->id}}>
                                                            <button class="btn btn-success">information</button>
                                                        </form></td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection




@section('js_special')

<!-- Page level custom scripts -->

<script>

</script>
<script >
    $(document).ready(function() {

        // Add Row
        $('#add-row').DataTable({
            "pageLength": 5,
        });

        var action = '<td> <div class="form-button-action"> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"> <i class="fa fa-edit"></i> </button> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove"> <i class="fa fa-times"></i> </button> </div> </td>';

        $('#addRowButton').click(function() {
            $('#add-row').dataTable().fnAddData([
                $("#addName").val(),
                $("#addPosition").val(),
                $("#addOffice").val(),
                action
                ]);
            $('#addRowModal').modal('hide');

        });
    });
</script>


@endsection









