@extends('layouts.app')

@section('contenu')
    <div class="main-panel">
        <div class="content">
            <div class="page-inner">
                <div class="page-header">
                    <h4 class="page-title">Choix du sujet de vote</h4>
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="{{ route('home') }}">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('sujetvote') }}">Choix sujet de vote</a>
                        </li>

                    </ul>
                </div>
                <form class="form" method="POST" action="{{route('choosesujet')}}">
                    @csrf
                    <div class="row">

                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="city-column">Sujet de vote</label>
                                    <select class="form-control" name="sujet" >
                                        <option value="" disabled>--Choix du sujet--</option>
                                            @foreach ($sujet as $sujet)
                                                <option value={{$sujet->id}}>{{$sujet->objet}}</option>
                                            @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 d-flex justify-content-end">
                                <button type="submit" class="btn btn-primary mr-1 mb-1">Valider</button>
                                <button type="reset" class="btn btn-light-secondary mr-1 mb-1">Annuler</button>
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>

@endsection




@section('js_special')

<!-- Page level custom scripts -->

<script>

</script>
<script >
    $(document).ready(function() {

        // Add Row
        $('#add-row').DataTable({
            "pageLength": 5,
        });

        var action = '<td> <div class="form-button-action"> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"> <i class="fa fa-edit"></i> </button> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove"> <i class="fa fa-times"></i> </button> </div> </td>';

        $('#addRowButton').click(function() {
            $('#add-row').dataTable().fnAddData([
                $("#addName").val(),
                $("#addPosition").val(),
                $("#addOffice").val(),
                action
                ]);
            $('#addRowModal').modal('hide');

        });
    });
</script>


@endsection









