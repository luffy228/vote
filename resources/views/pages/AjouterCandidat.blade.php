@extends('layouts.app')

@section('contenu')
    <div class="main-panel">
        <div class="content">
            <div class="page-inner">
                <div class="page-header">
                    <h4 class="page-title">Candidat</h4>
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="{{ route('home') }}">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('listevote')}}">Ajouter un candidat</a>
                        </li>

                    </ul>
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <form  action="{{route('addCandidat')}}" method="post">
                            @csrf
                            <input type="hidden" name="vote" value="{{$vote}}">
                            <input type="hidden" name="categorie" value="{{$categorie}}">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">Information sur le vote   </div>
                            </div>

                            <div class="card-body">

                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Nombre de candidat a ajouter</label>
                                    <select onchange="ecran($(this).val(),'information','GestionCandidatajax')" class="form-control" id="exampleFormControlSelect1" name="countCandidat">
                                        <option  selected disabled>0</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>

                                    </select>
                                </div>


                            </div>
                        </div>

                        <div class="card"  id="information">

                        </div>

                        <div class="card-action">
                            <button class="btn btn-success">Submit</button>
                            <button class="btn btn-danger">Cancel</button>
                        </div>
                    </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection




@section('js_special')

<!-- Page level custom scripts -->

<script>

</script>
<script >
    $(document).ready(function() {

        // Add Row
        $('#add-row').DataTable({
            "pageLength": 5,
        });

        var action = '<td> <div class="form-button-action"> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"> <i class="fa fa-edit"></i> </button> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove"> <i class="fa fa-times"></i> </button> </div> </td>';

        $('#addRowButton').click(function() {
            $('#add-row').dataTable().fnAddData([
                $("#addName").val(),
                $("#addPosition").val(),
                $("#addOffice").val(),
                action
                ]);
            $('#addRowModal').modal('hide');

        });
    });
</script>


@endsection









