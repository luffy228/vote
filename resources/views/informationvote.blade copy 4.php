@extends('layouts.app')



@section('contenu')
    <!--  ce qu'il y a de le content -->
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">Vote</h4>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="card card-profile card-secondary">
                        <div class="card-header" style="color: white">
                            <br>


                                    <h3>Intitule du vote: {{$information->vote->intitule}}</h3>
                        </div>
                        <div class="card-body">
                            <div class="user-profile text-center">
                                <div class="name">Type de vote :{{$information->vote->typeVote}}</div>
                                <div class="job">Type de votant :{{$information->vote->typeVotant}} </div>
                                <div class="job">Statut :  {{$information->vote->statut}}</div>
                                @if ($information->vote->typeVote_id == 1)
                                <div class="job">Sujet :  {{$information->sujet->nom}}</div>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
                @if ($information->vote->typeVote_id == 1)
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-head-row">
                                <div class="card-title">Resultats</div>
                                <div class="card-tools">
                                    <a href="#" class="btn btn-info btn-border btn-round btn-sm">
                                        <span class="btn-label">
                                            <i class="fa fa-print"></i>
                                        </span>
                                        Exporter
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="chart-container" style="min-height: 375px">
                                <canvas id="{{$information->vote->id}}"></canvas>
                            </div>
                            <div id="myChartLegend"></div>
                        </div>
                    </div>
                </div>
                @endif
                @if ($information->vote->typeVote_id == 2)
                <div class=" col-md-4">
                    <div class="card card-profile card-secondary">
                        <div class="card-header" style="color: white">
                            <br>


                                    <h3>Nombre de Categories</h3>
                        </div>
                        <div class="card-body">
                            <div class="user-profile text-center">
                                <div class="name">{{count($information->categorie)}}</div>


                            </div>
                        </div>
                    </div>
                </div>

                <div class=" col-md-4">
                    <div class="card card-profile card-secondary">
                        <div class="card-header" style="color: white">
                            <br>


                                    <h3>Nombre de candidats</h3>
                        </div>
                        <div class="card-body">
                            <div class="user-profile text-center">

                                <?php
                                                 $somme = 0 ;
                                                 foreach ($information->categorie as $categories) {
                                                     $somme += count($categories->candidat);
                                                 }
                                ?>
                                <div class="name">{{$somme}}</div>

                            </div>
                        </div>
                    </div>
                </div>

                @endif




            </div>

            @if ($information->vote->typeVote_id == 2)
            <div  class="row">
            @foreach ($information->categorie as $categorie)

                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-title fw-mediumbold">{{$categorie->nom}} <a href="{{route('addingCandidat',["vote"=>$information->vote->id , "categorie"=>$categorie->id ])}}">
                                <button class="btn btn-primary btn-round ml-auto">
                                    <i class="fa fa-plus"></i>
                                    Ajouter
                                </button>
                                </a></div>
                            <div class="card-list">
                                @foreach ($categorie->candidat as $candidat)
                                <div class="item-list">
                                    <div class="avatar">

                                    </div>
                                    <div class="info-user ml-3">
                                        <div class="username">{{$candidat->noms}}  {{$candidat->age}} ans  </div>

                                    </div>

                                </div>
                                @endforeach


                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-head-row">
                                <div class="card-title">Resultats de la categorie {{$categorie->nom}}</div>
                                <div class="card-tools">
                                    <a href="#" class="btn btn-info btn-border btn-round btn-sm">
                                        <span class="btn-label">
                                            <i class="fa fa-print"></i>
                                        </span>
                                        Exporter
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="chart-container" style="min-height: 375px">
                                <canvas id="{{$categorie->nom}}"></canvas>
                            </div>
                            <div id="myChartLegend"></div>
                        </div>
                    </div>
                </div>

            @endforeach
        </div>

            @endif


        </div>
    </div>

</div>

@endsection

@section('js_special')

<script>

    if ({{$information->vote->typeVote_id}} == 1) {
        var ctx = document.getElementById('{{$information->vote->id}}').getContext('2d');
        var statisticsChart = new Chart(ctx, {
	type: 'line',
	data: {
		labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
		datasets: [ {
			label: "Alertes",
			borderColor: '#f3545d',
			pointBackgroundColor: 'rgba(243, 84, 93, 0.2)',
			pointRadius: 0,
			backgroundColor: 'rgba(243, 84, 93, 0.1)',
			legendColor: '#f3545d',
			fill: true,
			borderWidth: 2,
			data: [154, 184, 175, 203, 210, 231, 240, 278, 252, 312, 320, 374]
		}, {
			label: "Alertes Resolus",
			borderColor: '#fdaf4b',
			pointBackgroundColor: 'rgba(253, 175, 75, 0.2)',
			pointRadius: 0,
			backgroundColor: 'rgba(253, 175, 75, 0.1)',
			legendColor: '#fdaf4b',
			fill: true,
			borderWidth: 2,
			data: [256, 230, 245, 287, 240, 250, 230, 295, 331, 431, 456, 521]
		}, ]
	},
	options : {
		responsive: true,
		maintainAspectRatio: false,
		legend: {
			display: false
		},
		tooltips: {
			bodySpacing: 4,
			mode:"nearest",
			intersect: 0,
			position:"nearest",
			xPadding:10,
			yPadding:10,
			caretPadding:10
		},
		layout:{
			padding:{left:15,right:15,top:15,bottom:15}
		},
		scales: {
			yAxes: [{
				ticks: {
					fontColor: "rgba(0,0,0,0.5)",
					fontStyle: "500",
					beginAtZero: false,
					maxTicksLimit: 5,
					padding: 20
				},
				gridLines: {
					drawTicks: false,
					display: false
				}
			}],
			xAxes: [{
				gridLines: {
					zeroLineColor: "transparent"
				},
				ticks: {
					padding: 20,
					fontColor: "rgba(0,0,0,0.5)",
					fontStyle: "500"
				}
			}]
		},
		legendCallback: function(chart) {
			var text = [];
			text.push('<ul class="' + chart.id + '-legend html-legend">');
			for (var i = 0; i < chart.data.datasets.length; i++) {
				text.push('<li><span style="background-color:' + chart.data.datasets[i].legendColor + '"></span>');
				if (chart.data.datasets[i].label) {
					text.push(chart.data.datasets[i].label);
				}
				text.push('</li>');
			}
			text.push('</ul>');
			return text.join('');
		}
	}
});

var myLegendContainer = document.getElementById("myChartLegend");

// generate HTML legend
myLegendContainer.innerHTML = statisticsChart.generateLegend();

// bind onClick event to all LI-tags of the legend
var legendItems = myLegendContainer.getElementsByTagName('li');
for (var i = 0; i < legendItems.length; i += 1) {
	legendItems[i].addEventListener("click", legendClickCallback, false);
};

    }else {





        if ({{$information->vote->typeVote_id}} == 2) {

var client = new Ably.Realtime("hf8FDg.EgVeqw:c6e5zZAau4rLutFt");

// ici je dois retourner le nombre de vote d'un user et j'incremente

const allcategorie =JSON.parse('{!! json_encode($information->categorie) !!}');
let nomCandidat = [];
let voteCandidat =[];

console.log(allcategorie);
client.connection.on('connected', function() {
        const receiveChannel = client.channels.get('testchannel');


  allcategorie.forEach(element => {
    var ctx = document.getElementById(element["nom"]).getContext('2d');

    receiveChannel.subscribe(function (message) {
        for (var i  = 0; i <element["candidat"].length; i++) {

nomCandidat.push(element["candidat"][i]["noms"]);
voteCandidat.push(10+i);
};
console.log(nomCandidat);
// console.log(voteCandidat);
console.log(voteCandidat[0]);
            if (message.name == element["nom"]) {
                voteCandidat[0] = voteCandidat[0] +1 ;
                console.log(voteCandidat[0]);
            }

            var statisticsChart = new Chart(ctx, {
type: 'bar',
data: {


labels:nomCandidat,
datasets: [
    {
    label: "Vote",
    borderColor: '#f3545d',
    pointBackgroundColor: 'rgba(243, 84, 93, 0.2)',
    pointRadius: 0,
    backgroundColor: 'rgba(243, 84, 93, 0.1)',
    legendColor: '#f3545d',
    fill: true,
    borderWidth: 2,
    data: voteCandidat
} ]
},
options : {
responsive: true,
maintainAspectRatio: false,
legend: {
    display: false
},
tooltips: {
    bodySpacing: 4,
    mode:"nearest",
    intersect: 0,
    position:"nearest",
    xPadding:10,
    yPadding:10,
    caretPadding:10
},
layout:{
    padding:{left:15,right:15,top:15,bottom:15}
},
scales: {
    yAxes: [{
        ticks: {
            fontColor: "rgba(0,0,0,0.5)",
            fontStyle: "500",
            beginAtZero: false,
            maxTicksLimit: 5,
            padding: 20
        },
        gridLines: {
            drawTicks: false,
            display: false
        }
    }],
    xAxes: [{
        gridLines: {
            zeroLineColor: "transparent"
        },
        ticks: {
            padding: 20,
            fontColor: "rgba(0,0,0,0.5)",
            fontStyle: "500"
        }
    }]
},
legendCallback: function(chart) {
    var text = [];
    text.push('<ul class="' + chart.id + '-legend html-legend">');
    for (var i = 0; i < chart.data.datasets.length; i++) {
        text.push('<li><span style="background-color:' + chart.data.datasets[i].legendColor + '"></span>');
        if (chart.data.datasets[i].label) {
            text.push(chart.data.datasets[i].label);
        }
        text.push('</li>');
    }
    text.push('</ul>');
    return text.join('');
}
}
});
statisticsChart.update();
nomCandidat.splice(0 , element["candidat"].length);
    voteCandidat.splice(0,element["candidat"].length+1);

  });









});





});




};

}
















</script>

@endsection





