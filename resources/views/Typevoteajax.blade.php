<div class="card-header">
    <div class="card-title">Contenu du vote</div>
</div>

@if ($val == 1)
<div class="card-body">
    <div class="form-group">
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text">Sujet du vote</span>
            </div>
            <textarea class="form-control" name="nomSujet" aria-label="With textarea"></textarea>
        </div>
    </div>
</div>

@endif

@if ($val == 2)
<div class="card-body">

    <div class="form-group">
        <label for="exampleFormControlSelect1">Nombre de categorie</label>
        <select  onchange="ecran($(this).val(),'informationCategorie','GestionCategorieajax')" name="countCategorie" class="form-control" id="exampleFormControlSelect1">
            <option selected disabled>choix du nombre de categorie</option>
            <option value="0">0</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
        </select>
    </div>

    <div class="form-group" id="informationCategorie">


    </div>
</div>

@endif
