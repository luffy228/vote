<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta charset="utf-8">
	<title>Vote Dashboard</title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="../assets/img/icon.ico" type="image/x-icon"/>

	<!-- Fonts and icons -->
	<script src="{{ asset('asset/js/plugin/webfont/webfont.min.js') }}"></script>
	<script>
		WebFont.load({
			google: {"families":["Open+Sans:300,400,600,700"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands"], urls: ["{{ asset('asset/css/fonts.css') }}"]},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<!-- CSS Files -->
	<link rel="stylesheet" href="{{ asset('asset/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('asset/css/azzara.min.css') }}">
    @yield('css_special')



</head>
<body>
	<div class="wrapper">
		<!--
			Tip 1: You can change the background color of the main header using: data-background-color="blue | purple | light-blue | green | orange | red"
		-->
		<div class="main-header" data-background-color="light-blue">
			<!-- Logo Header -->
			<div class="logo-header">

				<a href="{{ route('home') }}" class="logo">
					<img src="{{ asset('asset/img/embleme.png') }}" alt="navbar brand" class="navbar-brand" width="25%">
				</a>
				<button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon">
						<i class="fa fa-bars"></i>
					</span>
				</button>
				<button class="topbar-toggler more"><i class="fa fa-ellipsis-v"></i></button>
				<div class="navbar-minimize">
					<button class="btn btn-minimize btn-rounded">
						<i class="fa fa-bars"></i>
					</button>
				</div>
			</div>
			<!-- End Logo Header -->

			<!-- Navbar Header -->
			<nav class="navbar navbar-header navbar-expand-lg">

				<div class="container-fluid">


					<ul class="navbar-nav topbar-nav ml-md-auto align-items-center">


						<li class="nav-item dropdown hidden-caret">
							<a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false">
								<div class="avatar-sm">
									<img src="{{ asset('asset/img/user.png') }}" alt="utilisateur" class="avatar-img rounded-circle">
								</div>
							</a>
							<ul class="dropdown-menu dropdown-user animated fadeIn">
								<li>
									<div class="user-box">
										<div class="avatar-lg"><img src="{{ asset('asset/img/user.png') }}" alt="image profile" class="avatar-img rounded"></div>
										<div class="u-text">
											<p class="text-muted">                            {{$infoUser->phone}}
                                            </p><a href="#" class="btn btn-rounded btn-danger btn-sm">Voir le profil</a>
										</div>
									</div>
								</li>
								<li>
									<a class="dropdown-item" href="{{ route('logout') }}">Se deconnecter</a>
								</li>
							</ul>
						</li>

					</ul>
				</div>
			</nav>
			<!-- End Navbar -->
		</div>

		<!-- Sidebar -->
        @include('layouts.partials._nav')

		<!-- End Sidebar -->
        @yield('contenu')

	</div>
</div>
<!--   Core JS Files   -->
<script src="{{ asset('asset/js/core/jquery.3.2.1.min.js') }}"></script>
<script src="{{ asset('asset/js/core/popper.min.js') }}"></script>
<script src="{{ asset('asset/js/core/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="https://cdn.ably.io/lib/ably.min-1.js"></script>





<!-- jQuery UI -->
<script src="{{ asset('asset/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js') }}"></script>
<script src="{{ asset('asset/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js') }}"></script>

<!-- jQuery Scrollbar -->
<script src="{{ asset('asset/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js') }}"></script>


<!-- Moment JS -->
<script src="{{ asset('asset/js/plugin/moment/moment.min.js') }}"></script>


<!-- Chart JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.0/chart.min.js" type="text/javascript"></script>

<!-- jQuery Sparkline -->
<script src="{{ asset('asset/js/plugin/jquery.sparkline/jquery.sparkline.min.js') }}"></script>


<!-- Chart Circle -->
<script src="{{ asset('asset/js/plugin/chart-circle/circles.min.js') }}"></script>


<!-- Datatables -->
<script src="{{ asset('asset/js/plugin/datatables/datatables.min.js') }}"></script>


<!-- Bootstrap Notify -->
<script src="{{ asset('asset/js/plugin/bootstrap-notify/bootstrap-notify.min.js') }}"></script>


<!-- jQuery Vector Maps -->
<script src="{{ asset('asset/js/plugin/jqvmap/jquery.vmap.min.js') }}"></script>
<script src="{{ asset('asset/js/plugin/jqvmap/maps/jquery.vmap.world.js') }}"></script>


<!-- Google Maps Plugin -->
<script src="{{ asset('asset/js/plugin/gmaps/gmaps.js') }}"></script>



<!-- Azzara JS -->
<script src="{{ asset('asset/js/ready.min.js') }}"></script>


<!-- Azzara DEMO methods, don't include it in your project! -->
<script src="{{ asset('asset/js/setting-demo.js') }}"></script>
<script src="{{ asset('asset/js/demo.js') }}"></script>
<script>
    function ecran(val,idvu,fichier,param){
        var req = $.ajax({
            url:'{{URL::to('ecran')}}',
            type:'GET',
            data:{val: val,fichier: fichier,param: param},
            dataType:"html"
        });
        req.done(function(msg){
            $('#' + idvu).html(msg);
        });
    }
  </script>

@yield('js_special')
</body>

</html>
