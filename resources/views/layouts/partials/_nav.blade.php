<div class="sidebar">

    <div class="sidebar-background"></div>
    <div class="sidebar-wrapper scrollbar-inner">
        <div class="sidebar-content">
            <div class="user">
                <div class="avatar-sm float-left mr-1">
                    <img src="{{ asset('asset/img/user.png') }}" alt="utilisateur" class="avatar-img rounded-circle">
                </div>
                <div class="info">
                    <a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
                        <span>
                            {{$infoUser->phone}}
                            <span class="user-level">{{$infoRole}} </span>
                            <span class="caret"></span>
                        </span>
                    </a>
                    <div class="clearfix"></div>

                    <div class="collapse in" id="collapseExample">
                        <ul class="nav">
                            <li>
                                <a href="{{ route('profile') }}">
                                    <span class="link-collapse">Mon Profile</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <ul class="nav">
                <li class="nav-item active">
                    <a href="{{ route('home') }}">
                        <i class="fas fa-home"></i>
                        <p>Acceuil</p>
                    </a>
                </li>
                <li class="nav-section">
                    <span class="sidebar-mini-icon">
                        <i class="fa fa-ellipsis-h"></i>
                    </span>
                    <h4 class="text-section">Liens</h4>
                </li>

                @if ($infoRole == "SuperAdministrateur")
                    <li class="nav-item">
                        <a  href="{{route('listeadministrateur')}}">
                            <i class="fas   fa-user-plus"></i>
                            <p>Listes des administrateurs</p>
                        </a>
                    </li>
                @endif

                @if ($infoRole == "Administrateur")
                    <li class="nav-item">
                        <a  href="{{route('listevote')}}">
                            <i class="fas   fa-user-plus"></i>
                            <p>Listes des votes</p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a  href="#">
                            <i class="fas   fa-user-plus"></i>
                            <p>Listes des votants</p>
                        </a>
                    </li>
                @endif

                <li class="nav-item">
                    <a  href="{{ route('logout') }}">
                        <i class="fas fa-eject"></i>
                        <p>Deconnexion</p>
                    </a>
                </li>


            </ul>
        </div>
    </div>
</div>

<!--  <li class="nav-item">
                        <a  href="{{route('listevote')}}">
                            <i class="fas   fa-user-plus"></i>
                            <p>Listes des votes</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a  href="{{ route('listesujet') }}">
                            <i class="fas   fa-user-plus"></i>
                            <p>Listes des sujets</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a  href="{{ route('sujetvote') }}">
                            <i class="fas fa-inbox"></i>
                            <p>Vote</p>
                        </a>
                    </li>
                -->


