<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta charset="utf-8">
	<title>Se connecter</title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="{{ asset('asset/img/icon.ico') }}" type="image/x-icon"/>

	<!-- Fonts and icons -->
	<script src="{{ asset('asset/js/plugin/webfont/webfont.min.js') }}"></script>
	<script>
		WebFont.load({
			google: {"families":["Open+Sans:300,400,600,700"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands"], urls: ["{{ asset('asset/css/fonts.css') }}"]},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<!-- CSS Files -->
	<link rel="stylesheet" href="{{ asset('asset/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('asset/css/azzara.min.css') }}">
</head>
<body class="login">
	<div class="wrapper wrapper-login">
		<div class="container container-login animated fadeIn">

			<h3 class="text-center"><img src="#"> <br>Se conecter</h3>
			<div class="login-form">
                <form action="{{ route('admin') }}" method="post">
                    @csrf
                    <div class="form-group form-floating-label">
                        <input id="username" name="phone" type="text" class="form-control input-border-bottom" required>
                        <label for="username" class="placeholder">Nom d'utilisateur</label>
                    </div>
                    <div class="form-group form-floating-label">
                        <input id="password" name="password" type="password" class="form-control input-border-bottom" required>
                        <label for="password" class="placeholder">Mot de passe</label>
                        <div class="show-password">
                            <i class="flaticon-interface"></i>
                        </div>
                    </div>
                    <div class="row form-sub m-0">
                        <a href="#" class="link float-right">Mot de passe oublie ?</a>
                    </div>
                    <div class="form-action mb-3">
                        <button class="btn btn-primary btn-rounded btn-login">Se connecter</button>
                    </div>
                </form>
			</div>
		</div>
	</div>
	<script src="{{ asset('asset/js/core/jquery.3.2.1.min.js') }}"></script>
	<script src="{{ asset('asset/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js') }}"></script>
	<script src="{{ asset('asset/js/core/popper.min.js') }}"></script>
	<script src="{{ asset('asset/js/core/bootstrap.min.js') }}"></script>
	<script src="{{ asset('asset/js/ready.js') }}"></script>
</body>
</html>
