@extends('layouts.app')


@if ($infoRole == "SuperAdministrateur")
@section('contenu')
    <!--  ce qu'il y a de le content -->
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">Resume</h4>
            </div>
            <div class="row">
                <div style="width:75%;">
                    {!! $chartjs->render() !!}
                </div>

            </div>
        </div>
    </div>

</div>

@endsection

@endif

@if ($infoRole == "Administrateur")
@section('contenu')
    <!--  ce qu'il y a de le content -->
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">Resume</h4>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    <div class="card card-stats card-round">
                        <div class="card-body ">
                            <div class="row align-items-center">
                                <div class="col-icon">
                                    <div class="icon-big text-center icon-primary bubble-shadow-small">
                                        <i class="fas fa-users"></i>
                                    </div>
                                </div>
                                <div class="col col-stats ml-3 ml-sm-0">
                                    <div class="numbers">
                                        <p class="card-category">Candidats</p>
                                        <h4 class="card-title">1,294</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="width:75%;">
                        {!! $chartjs->render() !!}
                    </div>
            </div>
        </div>
    </div>

</div>

@endsection

@endif


