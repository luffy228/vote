<?php


use App\Http\Controllers\HomeController;
use App\Http\Controllers\web\AdministrateurController;
use App\Http\Controllers\web\AjaxController;
use App\Http\Controllers\web\GeneriqueController;
use App\Http\Controllers\web\SujetController;
use App\Http\Controllers\web\TypeVotantController;
use App\Http\Controllers\web\UserController;
use App\Http\Controllers\web\VoteController;
use Illuminate\Auth\Events\Login;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/admin',[HomeController::class,'adminPage'])->name('admin');
Route::get('/',[HomeController::class,'index'])->name('login');
Route::get('/logout',[HomeController::class,'logout'])->name('logout');




Route::prefix("/vote")->group(function () {
    Route::get('/home',[GeneriqueController::class,'index'])->name('home');
    Route::get('/profile',[GeneriqueController::class,'profile'])->name('profile');
    Route::post('/updatecompte',[GeneriqueController::class,'updateCompte'])->name('updatecompte');
    Route::post('/updatepassword',[GeneriqueController::class,'updatePassword'])->name('updatepassword');
});
Route::prefix("/superadmin")->group(function () {
    Route::get('/listDeputer', [UserController::class,'listDeputer'])->name('listedeputer');
    Route::post('/addDeputer',[UserController::class,'addDeputer'])->name('adddeputer');
    Route::get('/listSujet', [SujetController::class,'listSujet'])->name('listesujet');
    Route::post('/addSujet',[SujetController::class,'addSujet'])->name('addsujet');
    Route::get('/sujetvote',[SujetController::class,'sujetVote'])->name('sujetvote');
    Route::post('/ChooseSujet',[SujetController::class,'ChooseSujet'])->name('choosesujet');
    Route::get('/VoteOnline/{sujet}',[VoteController::class,'index'])->name('resultsujet');

    Route::post('/addTypeVotant',[TypeVotantController::class,'store'])->name('addtypevotant');
    Route::post('/addingvote',[VoteController::class,'store'])->name('addingvote');
    Route::get('/ajoutVote', [VoteController::class,'ajoutVote'])->name('addVote');





    Route::get('/listAdministrateur', [AdministrateurController::class,'index'])->name('listeadministrateur');
    Route::post('/addAdministrateur',[AdministrateurController::class,'store'])->name('addadmin');
    Route::get('/listVote', [VoteController::class,'index'])->name('listevote');
    Route::get('/addingCandidat/{vote}/{categorie}', [VoteController::class,'addingCandidat'])->name('addingCandidat');
    Route::post('/addCandidat', [VoteController::class,'addCandidat'])->name('addCandidat');
    Route::post('/informationvote', [VoteController::class,'information'])->name('informationvote');








});
Route::get('/ecran', [AjaxController::class,'ecran']);

//Auth::routes();


