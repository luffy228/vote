<?php

use App\Http\Controllers\api\SujetController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('/v1')->group(function () {
    Route::post('/login',[UserController::class,'login']);
});


Route::middleware(['auth:api'])->group( function () {
    Route::post('logout','UserController@logout');

});
// Pour seulement le role controlleur

Route::middleware(['auth:api','role:Deputer'])->prefix("/v1/deputer")->group(function () {
    Route::post('/canVote',[SujetController::class,'canVote']);
    Route::get('/listLois',[SujetController::class,'listSujet']);

});

